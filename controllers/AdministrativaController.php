<?php

class AdministrativaController {

    public function actionIndex() {

        $userId = Admin::checkLogged();
        $user = Admin::getAdminByID($userId);
        Admin::checkOnline($userId);
        require_once (ROOT . '/view/cabinet/indexAdmin.php');
        return true;
    }
    
    public function actionInfo(){
        $adminId = Admin::checkLogged();
        Admin::checkOnline($adminId);
        $admin = Admin::getData($adminId);
        
        require_once(ROOT.'/view/administrativa/info.php');
        return true;
    }

    public function actionTransDo() {
        $adminId = Admin::checkLogged();
        Admin::checkOnline($adminId);
        $transakce = Admin::getTransaction();

        if (( isset($_POST['ID']) ) && ( isset($_POST['ACCEPT']) )) {
            $transactionId = $_POST['ID'];
            if(!Admin::getTransStatus($transactionId)){
                Admin::acceptTransaction($transactionId, $adminId);
                $complete = TRUE;
            }else{
                $provedena = TRUE;
            }
        }
        if((isset($_POST['ID'])) && ( isset($_POST['DECLINE']) )){
            $transactionId = $_POST['ID'];
            if(!Admin::getTransStatus($transactionId)){
                Admin::declineTransaction($transactionId, $adminId);
                $complete = TRUE;
            }else{
                $provedena = TRUE;
            }
        }
        require_once (ROOT .'/view/administrativa/adminTransaction.php');
        return true;
    }
    public function actionAddAccount() {
       $users = User::getUsers();
       $adminId = Admin::checkLogged();
        Admin::checkOnline($adminId);
       if (isset($_POST['submitAcc'])) {
        $acc = $_POST['number'];
        $mena = $_POST['mena'];
        $limit = $_POST['limit'];
        $userId = $_POST['user'];
      } 
      $testAcc = Admin::checkAccount($acc);
     if($testAcc === true){
         $_SESSION['adm'.'acc'] = $_POST['number'];
     }
     else{
     if($testAcc === 'LENGHT_ERROR'){
            $errors[] = 'LENGHT ERROR(MAX 24)';
     }
          
     if($testAcc === 'PREFIX_ERROR'){
            $errors[] = 'PREFIX ERROR(NOT CZ/EU/US)';
     }
     if($testAcc === 'POSTFIX_ERROR'){
            $errors[] = 'POSTFIX ERROR(JUST NUMBERS)';
     }
    
     if($testAcc === 'ALREADY_EXIST'){
            $errors[] = 'ALREADY_EXIST(EXIST IN DB)';
     }
    }
     
     if(!Admin::checkLimit($limit)){
            $errors[] = 'Wrong limit format. MAX 11 symbols and only numbers';
     }
     else{
         $_SESSION['adm'.'limit'] = $_POST['limit'];         
     }
    
     if(empty($errors) && isset($_POST['submitAcc'])){
        $complete =  Admin::addUcet($acc, $mena, $limit, $userId);        
        Other::unsetSessionVar('adm','limit');
        Other::unsetSessionVar('adm','acc');
     }
    require_once(ROOT .'/view/administrativa/addAcc.php');
    return true;
    }
   
    public function actionAddUser(){
        $adminId = Admin::checkLogged();
        Admin::checkOnline($adminId);
        if(filter_input(INPUT_POST, 'submit')){
            $name = filter_input(INPUT_POST,'name');
            $surname = filter_input(INPUT_POST,'surname');
            $street = filter_input(INPUT_POST,'street');
            $town = filter_input(INPUT_POST,'town');
            $email = filter_input(INPUT_POST,'email');
            $psc = filter_input(INPUT_POST,'psc');
            $password = filter_input(INPUT_POST,'password');
            $phone = filter_input(INPUT_POST,'phone');
            $rc = filter_input(INPUT_POST,'rc');
            $value = array('name','surname','street','town','email','psc','password','phone','rc');
            $_SESSION['adm'.$value[0]] = $name;
            $_SESSION['adm'.$value[1]] = $surname;
            $_SESSION['adm'.$value[2]] = $street;
            $_SESSION['adm'.$value[3]] = $town;
            $_SESSION['adm'.$value[4]] = $email;
            $_SESSION['adm'.$value[5]] = $psc;
            $_SESSION['adm'.$value[6]] = $password;
            $_SESSION['adm'.$value[7]] = $phone;
            $_SESSION['adm'.$value[8]] = $rc;
        }else{
             for($i = 0 ; $i < count($value); $i++){
                Other:: unsetSessionVar('adm',$value[$i]);
            }
        }
                

        if(User::checkPassword($password) === 'LESS_PASSWORD'){ 
            $errors[] = 'LESS PASSWORD';
            Other::unsetSessionVar('adm',$value[6]);
        }
        //
        if(!empty($psc) && !Admin::formTest($psc, 5)){
            $errors[] = 'WRONG PSC FORMAT';
            Other::unsetSessionVar('adm',$value[5]);
        }
       
        if(!empty($phone) && !Admin::formTest($phone, 9)){
            $errors[] = 'WRONG PHONE FORMAT';
            Other::unsetSessionVar('adm',$value[7]);
        }
      
        if(!Admin::formTest($rc, 6)){
            $errors[] = 'WRONG RC FORMAT';
            Other:: unsetSessionVar('adm',$value[8]);
        }
        else if(!Admin::checkRc($rc,'klient')){
            $errors[] = 'RC ALREADY EXIST IN SYSTEM';
            Other:: unsetSessionVar('adm',$value[8]);
        }
            
        if(User::checkEmail($email) === 'ALREADY_EXIST'){
            $errors[] = 'ALREADY EXIST IN SYSTEM';
            Other:: unsetSessionVar('adm',$value[4]);
        }
        
         if(User::checkEmail($email) === 'WRONG_FORMAT'){
            $errors[] = 'WRONG EMAIL FORMAT';
            Other:: unsetSessionVar('adm',$value[4]);
        }
        
        
        if(empty($errors) && filter_input(INPUT_POST, 'submit')){
            $password = md5($password);
            Admin::addUser($name, $surname, $street, $town, $email, $psc, $password, $phone, $rc);
            $complete = TRUE;
            for($i = 0 ; $i < count($value); $i++){
                Other:: unsetSessionVar('adm',$value[$i]);
            }
         }
        require_once (ROOT.'/view/administrativa/addUser.php');
        return true;
    }
    public function actionStatus(){
        $adminId = Admin::checkLogged();
        Admin::checkOnline($adminId);
        
        $account = Admin::getAccounts();
        $accNo = $_POST['user'];
        $status = $_POST['status'];
        $tmp = Admin::changeStatus($accNo, $status);
        if(!$tmp){
            $errors[] = 'This accaunt already has this status!';
        }
        else{
            
            $complete = TRUE;
        }
        require_once(ROOT.'/view/administrativa/status.php');
        return true;
    }
    
    public function actionTransactionView(){
        $status = $_POST['status'];
        if( ( isset( $_POST['od'] ) ) && ( empty ( $_POST['do'] ) ) && ( !empty( $_POST['od'] ) ) ){
            
            $transakce = Admin::dateTest($_POST['od'], 0,$status);
        }
        else if( ( isset($_POST['do'] ) ) && ( empty($_POST['od'] ) ) && ( !empty( $_POST['do'] ) ) ){
   
            $transakce = Admin::dateTest(0, $_POST['do'],$status);
        }
        else if( isset( $_POST['od'] ) && isset( $_POST['do'] ) && ( !empty( $_POST['do'] ) ) && ( !empty( $_POST['od'] ) ) ){
            
            $transakce = Admin::dateTest($_POST['od'], $_POST['do'],$status);
        }
        else{
            
            $transakce = Admin::getAllTransaction($status);
        }
        require_once(ROOT.'/view/administrativa/transaction.php');
        return true;
    }
    
    public function actionDeleteUser(){
        $adminId = Admin::checkLogged();
        Admin::checkOnline($adminId);
        
        $users = User::getUsers();
        if(isset($_POST['user']) && isset($_POST['submit'])){
            $id = $_POST['user'];
            Administrator::deleteUser($id);
            $complete = TRUE;
        }
        require_once(ROOT.'/view/administrativa/deleteU.php');
        return true;
    }
    
    public function actionDeleteAccount(){
        $adminId = Admin::checkLogged();
        Admin::checkOnline($adminId);
        $accounts = Admin::getAccounts();
        if(isset($_POST['account']) && isset($_POST['submit'])){
            $id = $_POST['account'];
            Administrator::deleteAccount($id);
            $complete = TRUE;
        }        
        require_once(ROOT.'/view/administrativa/deleteAccount.php');
        return true;
    }
}
