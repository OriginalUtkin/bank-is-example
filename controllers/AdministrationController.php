<?php

class AdministrationController {
    
    public function actionLogin(){ 

        if(isset($_POST['submit']) ){
            $email = $_POST['email'];
            $password = $_POST['password'];
            $errors = false;
        }
        $aId = Administrator::checkData($email, $password); 
        
        if($aId == false){
            $errors[] = "Wrong data";
        } 
        else{
            Administrator::auth($aId);
            header("Location:/aIndex");
        }
        
        require_once(ROOT.'/view/login/loginAdministrator.php');
        return true;
    }
    
    public function actionIndex(){
        $adminId = Administrator::checkLogged();
        
        require_once(ROOT.'/view/admin/index.php');
        return true;
    }


    public function actionLogout(){
        session_start();
        unset($_SESSION['administrator']);
        header("Location: /");
    }
    
    public function actionAddP(){
        $aId = Administrator::checkLogged();
        $value = array('name','surname','street','town','email','psc','password','phone','rc');
        if(filter_input(INPUT_POST, 'submit')){
            $name = filter_input(INPUT_POST,'name');
            $surname = filter_input(INPUT_POST,'surname');
            $street = filter_input(INPUT_POST,'street');
            $town = filter_input(INPUT_POST,'town');
            $email = filter_input(INPUT_POST,'email');
            $psc = filter_input(INPUT_POST,'psc');
            $password = filter_input(INPUT_POST,'password');
            $phone = filter_input(INPUT_POST,'phone');
            $rc = filter_input(INPUT_POST,'rc');
            $_SESSION['p'.$value[0]] = $name;
            $_SESSION['p'.$value[1]] = $surname;
            $_SESSION['p'.$value[2]] = $street;
            $_SESSION['p'.$value[3]] = $town;
            $_SESSION['p'.$value[4]] = $email;
            $_SESSION['p'.$value[5]] = $psc;
            $_SESSION['p'.$value[6]] = $password;
            $_SESSION['p'.$value[7]] = $phone;
            $_SESSION['p'.$value[8]] = $rc;
        }else{
            for($i = 0 ; $i < count($value); $i++){
                Other:: unsetSessionVar('p',$value[$i]);
            }
        }               
                
        if(User::checkPassword($password) === 'LESS_PASSWORD'){ 
            $errors[] = 'LESS PASSWORD';
            Other:: unsetSessionVar('p',$value[6]);
        }
   
        //PSC TEST
        if(!empty($_POST['psc'])&&!Admin::formTest($psc, 5)){
            $errors[] = 'WRONG PSC FORMAT';
            Other:: unsetSessionVar('p',$value[5]);
        }
        
        if(!empty($_POST['phone'])&&!Admin::formTest($phone, 9)){
            $errors[] = 'WRONG PHONE FORMAT';
            Other:: unsetSessionVar('p',$value[7]);
        }
        
        //RC TEST
        if(!Admin::formTest($rc, 6)){
            $errors[] = 'WRONG RC FORMAT';
            Other:: unsetSessionVar('p',$value[8]);
        }
        else if(!Admin::checkRc($rc,'pracovnik')){
            $errors[] = 'RC ALREADY EXIST IN SYSTEM';
            Other:: unsetSessionVar('p',$value[8]);
        }
        
        //EMAIL TEST
        if(Administrator::checkEmail($email) === 'ALREADY_EXIST'){
            $errors[] = 'ALREADY EXIST IN SYSTEM';
            Other:: unsetSessionVar('p',$value[4]);
        }
        
         else if(User::checkEmail($email) === 'WRONG_FORMAT'){
            $errors[] = 'WRONG EMAIL FORMAT';
            Other:: unsetSessionVar('p',$value[4]);
        }
                
        if(empty($errors) && filter_input(INPUT_POST, 'submit')){
           $password = md5($password);
            Administrator::addPracovnik($name, $surname, $street, $town, $email, $psc, $password, $phone, $rc);
            $complete = TRUE;
            for($i = 0 ; $i < count($value); $i++){
                Other:: unsetSessionVar('p',$value[$i]);
            }
         }
        require_once(ROOT.'/view/admin/addP.php');
        return true;
    }
    
    public function actionDeleteP(){
        $aId = Administrator::checkLogged();

        $allP = Administrator::getPracovnik();
        if(isset($_POST['pracovnik']) && isset($_POST['submit'])){
            $id = $_POST['pracovnik'];
            Administrator::deletePracovnik($id);
            $complete = TRUE;
        }
        require_once(ROOT.'/view/admin/deleteP.php');
        return true; 
    }
    
    public function actionAddUser(){
        $aId = Administrator::checkLogged();
        $value = array('name','surname','street','town','email','psc','password','phone','rc');
        if(filter_input(INPUT_POST, 'submit')){
            $name = filter_input(INPUT_POST,'name');
            $surname = filter_input(INPUT_POST,'surname');
            $street = filter_input(INPUT_POST,'street');
            $town = filter_input(INPUT_POST,'town');
            $email = filter_input(INPUT_POST,'email');
            $psc = filter_input(INPUT_POST,'psc');
            $password = filter_input(INPUT_POST,'password');
            $phone = filter_input(INPUT_POST,'phone');
            $rc = filter_input(INPUT_POST,'rc');

            $_SESSION['u'.$value[0]] = $name;
            $_SESSION['u'.$value[1]] = $surname;
            $_SESSION['u'.$value[2]] = $street;
            $_SESSION['u'.$value[3]] = $town;
            $_SESSION['u'.$value[4]] = $email;
            $_SESSION['u'.$value[5]] = $psc;
            $_SESSION['u'.$value[6]] = $password;
            $_SESSION['u'.$value[7]] = $phone;
            $_SESSION['u'.$value[8]] = $rc;
        }else{
            for($i = 0 ; $i < count($value); $i++){
                Other:: unsetSessionVar('u',$value[$i]);
            }
        }
        
        if(User::checkPassword($password) === 'LESS_PASSWORD'){ 
            $errors[] = 'LESS PASSWORD';
            Other:: unsetSessionVar('u',$value[6]);
        }
        
        if(!empty($psc) && !Admin::formTest($psc, 5)){
            $errors[] = 'WRONG PSC FORMAT';
            Other:: unsetSessionVar('u',$value[5]);
        }
        
        if(!empty($phone) && !Admin::formTest($phone, 9)){
            $errors[] = 'WRONG PHONE FORMAT';
            Other:: unsetSessionVar('u',$value[7]);
        }
        
        if(!Admin::formTest($rc, 6)){
            $errors[] = 'WRONG RC FORMAT';
            Other:: unsetSessionVar('u',$value[8]);
        }
        else if(!Admin::checkRc($rc,'klient')){
            $errors[] = 'RC ALREADY EXIST IN SYSTEM';
            Other:: unsetSessionVar('u',$value[8]);
        }
                           
        if(User::checkEmail($email) === 'ALREADY_EXIST'){
            $errors[] = 'EMAIL ALREADY EXIST IN SYSTEM';
            Other:: unsetSessionVar('u',$value[4]);
        }
        
         else if(User::checkEmail($email) === 'WRONG_FORMAT'){
            $errors[] = 'WRONG EMAIL FORMAT';
            Other:: unsetSessionVar('u',$value[4]);
        }
                        
        if(empty($errors) && filter_input(INPUT_POST, 'submit')){
            $password = md5($password);
            Admin::addUser($name, $surname, $street, $town, $email, $psc, $password, $phone, $rc);
            $complete = TRUE;
            for($i = 0 ; $i < count($value); $i++){
                Other:: unsetSessionVar('u',$value[$i]);
            }
         }
        require_once(ROOT.'/view/admin/addU.php');
        return true; 
    }
    
    public function actionDeleteUser(){
        $aId = Administrator::checkLogged();
        $users = User::getUsers();
        if(isset($_POST['user']) && isset($_POST['submit'])){
            $id = $_POST['user'];
            Administrator::deleteUser($id);
            $complete = TRUE;
        }
        require_once(ROOT.'/view/admin/deleteU.php');
        return true; 
    }



    public function  actionClearDb(){
        $columns = Administrator::tableColumns();
        
          if(isset($_POST['table']) && isset($_POST['submit'])){
              $table = $_POST['table'];
              Administrator::deleteTable($table);
          }
        return true;
    }
    
    public function actionBalanceAdd(){
        $accounts = Admin::getAccounts();
        if(isset($_POST['submit'])){
            $account = $_POST['account'];
            $balance = $_POST['balance'];
            if(Administrator::changeBalance($account, $balance)){
                $complete = TRUE;
            }else{
                $errors[] = 'So much for this account';
            }
            
        }
        require_once(ROOT.'/view/admin/balanceAdd.php');
        return true;
    }
}
