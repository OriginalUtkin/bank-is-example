<?php
include_once (ROOT.'/controllers/SiteController.php');

class AdminController {
    public function actionLogin(){ 
        if(!User::checkTimeout()) {
            SiteController::accessDenied(); 
        }
        
        if(!User::isGuest()){
            header("Location: /cabinet");
        }else if(!Admin::isGuest()){
            header("Location: /administrativa");
        }
        
        if(isset($_POST['submit'])){
            $email = $_POST['email'];
            $password = md5($_POST['password']);
            $errors = false;
            $adminId = Admin::checkAdminData($email, $password);
        
        
            if($adminId== false){
                $_SESSION['timeout'] = User::timeout();
                 if($_SESSION['timeout'] === TRUE){
                    
                    $errors[] = 'Wrong data';
                }
                else{
                    SiteController::accessDenied();  
                }                        
            }   
            else{
               Admin::auth($adminId);
               header("Location: /administrativa");
            }
        }
        
        require_once(ROOT.'/view/login/loginAdmin.php');
        return true;
    }
    
     public function actionLogout(){
        session_start();
        $adminId = $_SESSION['admin'] ;
        unset($_SESSION['admin']);
        $db = Db::getConnection();
        $sql = "DELETE FROM statusp WHERE id = $adminId";
        $result2 = $db->prepare($sql);
        $result2->execute();
        header("Location: /");
    }
}
