<?php

class CabinetController
{
    public function actionIndex()
    {
        $userId = User::checkLogged();
        User::checkOnline($userId);
        $user = User::getUserByID($userId);
        require_once (ROOT.'/view/cabinet/index.php');
        return true;
    }

    public function actionInfo()
    {
        $userId = User::checkLogged();
        User::checkOnline($userId);
        $user = User::getUserByID($userId);
        $ucet = User::getUserUcetByID($userId);
        require_once(ROOT.'/view/cabinet/info.php');
        return true;
    }
    
    public function actionTransaction(){
        $userId = User::checkLogged();
        User::checkOnline($userId);
        
        if( ( isset( $_POST['od'] ) ) && ( empty ( $_POST['do'] ) ) ){
            $transakce = User::dateTest($_POST['od'], 0,$userId);
        }
        else if( ( isset($_POST['do'] ) ) && ( empty($_POST['od'] ) ) ){
            $transakce = User::dateTest(0, $_POST['do'],$userId);
        }
        else if( isset( $_POST['od'] ) && isset( $_POST['do'] ) ){
            $transakce = User::dateTest($_POST['od'], $_POST['do'],$userId);
        }
        else{
            $transakce = User::getTransaction($userId);
        }
        require_once(ROOT.'/view/cabinet/transaction.php');
        return true;
    }
    
    public function actionOperation(){
        
        $userId = User::checkLogged();
        User::checkOnline($userId);
        $value = array('fromAcc','toAcc','castka');
        if(isset($_POST['submit'])){
            $fromAcc = $_POST['fromAcc'];
            $toAcc = $_POST['toAcc'];
            $sendMoney = $_POST['castka'];
            $_SESSION['cabinet'.$value[0]] = $fromAcc;
            $_SESSION['cabinet'.$value[1]] = $toAcc;
            $_SESSION['cabinet'.$value[2]] = $sendMoney;
        }else{
            for($i = 0 ; $i < count($value); $i++){
                  Other::unsetSessionVar('cabinet',$value[$i]);
            }
        }
                
        if(!Operations::checkAccount($userId,$fromAcc)){
                $errors[] = "You are not owner this account";
                 Other::unsetSessionVar('cabinet',$value[0]);
        }
        if(!Operations::checkBalance($fromAcc,$sendMoney)){
            if(empty($errors)){
                $errors[] = "You have't money on this account";
                Other::unsetSessionVar('cabinet',$value[0]);
            }
        }
        if(!Operations::checkTargetAccount($toAcc)){
            $errors[] = "Target account doesn't exist in system or closed";
            Other::unsetSessionVar('cabinet',$value[1]);
        }
          
       if(!Operations::checkLimit($toAcc,$sendMoney)){
            $errors[] = "Target account has a enemy type and cant get so much";
            Other::unsetSessionVar('cabinet',$value[1]);
        } 
    
        if(!Admin::checkLimit($sendMoney)){
            $errors[] = "Wrong format for castka";
            Other::unsetSessionVar('cabinet',$value[2]);
        }
        if(!Operations::checkMena($toAcc,$fromAcc)){
            $errors[] = "Different currencies in accounts";
            Other::unsetSessionVar('cabinet',$value[0]);
            Other::unsetSessionVar('cabinet',$value[1]);
        }
        
        if(($errors == false) && (isset($_POST['submit']))){
            Operations::addTransaction($sendMoney,$userId,$fromAcc,$toAcc);
            
            User::getUserEmail($toAcc);
            self::sendMail($sendMoney,$fromAcc,$toAcc);
            for($i = 0 ; $i < count($value); $i++){
                  Other::unsetSessionVar('cabinet',$value[$i]);
            }
        }
        require_once(ROOT.'/view/cabinet/operation.php');
        return true;
    }
    
    public function actionEdit(){
        $userId = User::checkLogged();
        User::checkOnline($userId);
        if(isset($_POST['submit'])){
            if(isset($_POST['email']) && !empty($_POST['email'])){
                $email = $_POST['email'];
                $test = User::checkEmail($email);
                if($test == 'OK'){
                   User::editEmail($userId, $email);
                   $complete = TRUE;
                }
                else if($test == 'ALREADY_EXIST')
                {
                    $errors[] = "This email already exist in system";
                }
                else{
                    $errors[] = "Wrong format email adress";
                }
            }
            if(isset($_POST['password']) && !empty($_POST['password'])){
                $password = $_POST['password'];
                $test = User::checkPassword($password);
               
                if($test == 'OK'){
                    User::editPassword($userId, $password);
                    $complete = TRUE;
                }
                else{
                   $errors[] = "Password can't be less than 6 symbols"; 
                }
            }
            
            if(isset($_POST['ulice']) && !empty($_POST['ulice'])){
                $street = $_POST['ulice'];
                User::edit($userId,'ulice',$street);
                $complete = TRUE;
            }
            
            if(isset($_POST['mesto']) && !empty($_POST['mesto'])){
                $town = $_POST['mesto'];
                User::edit($userId,'mesto',$town);
                $complete = TRUE;
            }
            
            if((isset($_POST['telefon'])) && !empty($_POST['telefon'])){
                $phone = $_POST['telefon'];
                if(!Admin::formTest($phone,9)){
                    $errors[] = "Invalid phone number";
                }
                else{
                    User::edit($userId, 'telefon', $phone);
                    $complete = TRUE;
                }
            }
            
            if(isset($_POST['psc']) && !empty($_POST['psc'])){
                $psc = $_POST['psc'];
                if(!Admin::formTest($psc, 5)){
                    $errors[] = "Invalid value psc";
                }
                else{
                    User::edit($userId, 'psc', $psc);
                    $complete = TRUE;
                }
            }
        }
        require_once(ROOT.'/view/cabinet/edit.php');
        return true;
    }
    
    public function actionCredits(){
        $userId = User::checkLogged();
        $accounts = User::getUserUcetByID($userId);
        $money =  User::getAllBalance($userId);
        require_once(ROOT.'/view/cabinet/credits.php');
        return true;
    }
 
   public function sendMail($castka,$from,$to){
       $email = User::getUserEmail($to);
       $subject = "TRANSACTION TO YOUR ACCOUNT";
       $message = "Byla zadana ve systemu transakce do vaseho accountu $to z uctu $from s castkou $castka";
       mail($email, $subject, $message);
        
    }
}