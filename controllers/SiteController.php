<?php
class SiteController{
    public function actionIndex(){
       
        require_once(ROOT.'/view/mainPage/index.php');
        return true;
    }

    public function actionLogin(){
        if(!User::isGuest()){
            header("Location: /cabinet");
        }else if(!Admin::isGuest()){
            header("Location: /administrativa");            
        }
        require_once (ROOT.'/view/login/loginTmp.php');
        return true;
    }
    public function actionTask(){
        require_once (ROOT.'/view/mainPage/task.php');
        return true;
    }
    
      public function actionContact(){
        require_once (ROOT.'/view/mainPage/contactus.php');
        return true;
    }


    public function actionError(){
        require_once (ROOT.'/view/Errors/404.php');
        exit(1);
    }
    public function accessDenied(){
        require_once (ROOT.'/view/Errors/AD.php');
        exit(1);
    }
    
}

