<?php
include_once (ROOT.'/controllers/SiteController.php');
class UserController{    
public function actionLogin()
    {     
        if(!User::checkTimeout()) {
            SiteController::accessDenied(); 
        }
        
        if(!User::isGuest()){
            header("Location: /cabinet");
        }else if(!Admin::isGuest()){
            header("Location: /administrativa");            
        }
            
        if(isset($_POST['submit'])){
         
            $email = $_POST['email'];
            $password = md5($_POST['password']);
            $errors = false;
            $userId = User::checkUserData($email, $password);
                        
            if($userId == false){
                $_SESSION['timeout'] = User::timeout();           
                if($_SESSION['timeout'] === TRUE){
                    $errors[] = 'Wrong data';
                }
                else{
                    SiteController::accessDenied();  
                }                         
            }
            else{
                User::auth($userId);
                header("Location: /cabinet");                                                                   
            }
        }               
        require_once(ROOT.'/view/login/loginUser.php');
        return true;
    }
   
    public function actionLogout(){
        session_start();
        $userId = $_SESSION['user'];
        unset($_SESSION["user"]);
        $db = Db::getConnection();
        $sql = "DELETE FROM status WHERE id = $userId";
        $result2 = $db->prepare($sql);
        $result2->execute();
        header("Location: /");
    }
        
    
}