<?php

return array(
        'administrativa/addUser'=>'administrativa/addUser',
        'administrativa/deleteUser'=>'administrativa/deleteUser',
        
	
        'administration/login'=>'administration/login',
        'administration/logout'=>'administration/logout',
        'user/login'=>'user/login',
        'admin/login'=>'admin/login',
        'user/logout'=>'user/logout',
        'admin/logout'=>'admin/logout',
        'login'=>'site/login',
    
       
        'cabinet/operation'=>'cabinet/operation',
        'cabinet/transaction'=>'cabinet/transaction',
        'cabinet/info'=>'cabinet/info',
        'cabinet/edit'=>'cabinet/edit',
        'cabinet/credits'=>'cabinet/credits',
        'cabinet/deleteU'=>'cabinet/deleteUser',
        'cabinet'=>'cabinet/index',
        
        
        'balanceAdd'=>'administration/balanceAdd',
        'aIndex' => 'administration/index',
        'addPracovnik'=>'administration/addP',
        'deletePracovnik'=>'administration/deleteP',
        'addU'=>'administration/addUser',
        'deleteU'=>'administration/deleteUser',
        'administrator/logout'=>'administrator/logout',
        
       
        'administrativa/deleteAccount'=>'administrativa/deleteAccount',
        'administrativa/info'=>'administrativa/info',
        'administrativa/transDo'=>'administrativa/transDo',
        'administrativa/addAccount'=>'administrativa/addAccount',
        'administrativa/transaction'=>'administrativa/transactionView',        
        'administrativa/status'=>'administrativa/status',
        'administrativa'=>'administrativa/index',
        

        'index.php'=>'site/index',
        ''=>'site/index',
	);