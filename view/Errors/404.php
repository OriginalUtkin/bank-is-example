<!DOCTYPE HTML>
<html>
    <head>
        <title>404 PAGE NOT FOUND</title>
        <link href='http://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister' rel='stylesheet' type='text/css'>
        <link href="/template/css/style404.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="wrap">
            <div class="logo">
			<p>OOPS! - Could not Find it</p>
                        <img src="/template/images/404-1.png"/>
			<div class="sub">
			  <p><a href="/">Back To Home Page </a></p>
			</div>
            </div>
        </div>	
    </body>
</html>