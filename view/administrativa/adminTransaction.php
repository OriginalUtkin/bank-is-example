<?php include (ROOT.'/view/layouts/header.php'); ?>
<div class="menu-wrap">
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li>
      <li><a href="#" >The Task</a></li>
      <li><a href="/cabinet"  class="active">Cabinet</a></li>
      <li style="background-color: black; margin-left: 49%; border-color: black;"><a href="/admin/logout">Logout</a></li>
    </ul>
  </div>
</div>

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1><span>TRANSACTION ADMIN</span></h1>
  </div>
</div>

<div class="page">
    <!-- Успех -->
    <?php if($complete):?> 
    <h3><img src="/template/images/newimages/ok.png" class="okimg"><?php echo "Status change.Please,refresh page"; ?></h3><br>
    <?php endif; ?>
    <!-- Ошибка -->
     <form action ="" method="POST">
         <input  placeholder="Transaction number"   name="ID" type="text" class="tranbumb">  
        <input  value="ACCEPT" name="ACCEPT" type="submit" class="buttonac">
        <input  value="DECLINE" name="DECLINE" type="submit" class="buttondec">
    </form>
    <a href="/administrativa"><img src="/template/images/newimages/back.png" class="backbutt"></a>
    <br>
    <div class="generic transaction">
      <div class="panel" style="width: 100%; display: inline-block;">
      <?php if(!empty($transakce)):?>
      <?php for($i = 0; $i < count($transakce);$i++ ): ?>
          <div style="float: left; margin-left: 3%; margin-bottom: 20px; background-color: #E6E9F1;">
            <div class="title">
                <h1>TRANSACTION NUMBER:<?php echo $transakce[$i]['cisloTransakce']; ?> </h1>
            </div>
            <hr style="width: 75%; margin-left: 13%;">
            <br>
            <div class="content" style="height: 220px; text-align:left;padding: 10px;">
                Operation:<?php echo $transakce[$i]['typ'];?><br>
                <?php $mena = Operations::getMena($transakce[$i]['cislo_uctu']) ?><br>
                Amount:&nbsp;<?php echo $transakce[$i]['castka']." ".$mena['mena']; ?><br> <br>
                Date:&nbsp <?php echo $transakce[$i]['datum']; ?><br><br>
                From acc: &nbsp <span style="border: 1px solid #B1B6C4; padding: 5px; margin-bottom: 40px;"><?php echo $transakce[$i]['zUctu']; ?></span><br/><br>
                To acc: &nbsp <span style="border: 1px solid #B1B6C4; margin-left: 23px; padding: 5px;"><?php echo $transakce[$i]['cislo_uctu']; ?></span> <br><br>
           </div>
      </div>
      <?php endfor; ?>
    <?php else: ?>
    </div>
  </div>
        <div class="generic transaction">
            <div class="panel">
                <div class="title">
                    <h1>You have't transaction</h1>
                </div>
            </div>
        </div>
    <?php endif;?>
</div>


<?php include (ROOT.'/view/layouts/footer.php'); ?>