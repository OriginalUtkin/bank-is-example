 <?php include (ROOT.'/view/layouts/header.php');?>
<div class="menu-wrap">
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li>
      <li><a href="#" >The Task</a></li>
      <li><a href="/administrativa"  class="active">Cabinet</a></li>
      <li style="background-color: black; margin-left: 49%; border-color: black;"><a href="/admin/logout">Logout</a></li>
    </ul>
  </div>
</div>

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>FIT<span>BANK SYSTEM</span></h1>
  </div>
  
</div>


<div class="page">
<a href="/administrativa"><img src="/template/images/newimages/back.png" class="backbutt"></a>
<br>
  <div class="generic">
    <div class="panel">
      <div class="title">
        <h1>DELETE USER FROM SYSTEM</h1>
      </div>
      <hr>  
      <div class="content">
              <form action =""  method="POST">
              <div class="noterror"><?php if($complete == TRUE):?> 
               <img src="/template/images/newimages/ok.png" class="okimg"><?php echo"USER DELETED FROM DATABASE. PLEASE,REFRESH PAGE";  ?>
              <?php endif; ?></div><br/>
              
              Choose user from system<br/><br/>
              <select name="user">
                  <?php for($i = 0; $i < count($users); $i++): ?>
                    <option value = "<?php echo $users[$i]['cisloKlienta']; ?>"> <?php echo $users[$i]['prijmeni']." "."|"." ".$users[$i]['rodne_cislo']; ?> </option>
                  <?php endfor; ?>
                </select><br/><br/>
            <input style="width: 140px; height: 30px; background: #202431; color: #ffffff; border-top-color: #9fb5b5;border-left-color: #608586;border-bottom-color: #1b4849;border-right-color: #1e4d4e;" value="DELETE" name="submit" type="submit">
        </form><br/><br/>
     </div>
    </div>
  </div>
</div>
<?php include (ROOT.'/view/layouts/footer.php');

