 <?php include (ROOT.'/view/layouts/header.php');?>
<div class="menu-wrap">
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li>
      <li><a href="#" >The Task</a></li>
      <li><a href="/administrativa"  class="active">Cabinet</a></li>
      <li style="background-color: black; margin-left: 49%; border-color: black;"><a href="/admin/logout">Logout</a></li>
    </ul>
  </div>
</div>

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>FIT<span>BANK SYSTEM</span></h1>
  </div>
  
</div>

<div class="page">
<a href="/administrativa"><img src="/template/images/newimages/back.png" class="backbutt"></a>
<br>
  <div class="generic">
    <div class="panel">
      <div class="title">
        <h1>ADD ACCOUNT</h1>
      </div>
      <hr>
      <br>
      <div class="content">

        <?php if ( (isset($errors)) && (is_array($errors)) && (isset($_POST['submitAcc'])) ): ?>
          <div class="error" > 

            <ul>
                <?php foreach ($errors as $error): ?>
                    <li> <h3><img src="/template/images/newimages/notok.png" class="notokimg">&nbsp <?php echo $error; ?></h3></li>
                <?php endforeach; ?>
            </ul>
        <?php endif;?>
      </div>

      <div class="noterror">     
        <?php if($complete):?>
         <img src="/template/images/newimages/ok.png" class="okimg">ACCAUNT OPENNED IN SYSTEM
        <?php endif; ?>
        </div>

        <div class="editadmin">
          <form action =""  method="POST" class="addacc">
              Choose user from system<br/>
              <select name="user">
                  <?php for($i = 0;$i<count($users);$i++): ?>
                  <option value = "<?php echo $users[$i]['cisloKlienta']; ?>"> <?php echo $users[$i]['prijmeni']."|".$users[$i]['rodne_cislo']; ?> </option>
                  <?php endfor; ?>
                </select><br/><br>
                
              Enter account number:<br/>
              <input style="width: 230px;border: 1px solid;background-color: #1a202c;padding: 8px 24px 8px 10px;letter-spacing: .075em;color: #ffffff;text-shadow: 0 1px 0 rgba(0,0,0,.1);margin-bottom: 19px;" placeholder="*Account Number" required  name="number" type="text" value="<?php if(isset($_SESSION['adm'.'acc'])): ?><?php echo $_SESSION['adm'.'acc']?><?php endif; ?>"><br/>
            Choose mena:<br/>
            <select name="mena">
                <option value="CZK">CZK</option>
                <option value="EUR">EUR</option>
                <option value="USD">USD</option>
            </select><br/>
            Enter limit for account:<br/>
            <input style="width: 230px;border: 1px solid;background-color: #1a202c;padding: 8px 24px 8px 10px;letter-spacing: .075em;color: #ffffff;text-shadow: 0 1px 0 rgba(0,0,0,.1);margin-bottom: 19px;" class="formlogadd" placeholder="*Account limit" required name="limit" value="<?php if(isset($_SESSION['adm'.'limit'])): ?><?php echo $_SESSION['adm'.'limit']?><?php endif; ?>" type="text"><br/><br>
            <input style="width: 140px; height: 30px; background: #202431; color: #ffffff; border-top-color: #9fb5b5;border-left-color: #608586;border-bottom-color: #1b4849;border-right-color: #1e4d4e;" value="ADD" name="submitAcc" type="submit">
        </form><br/><br/>
        </div>
     </div>
    </div>
  </div>
</div>
<?php include (ROOT.'/view/layouts/footer.php');