 <?php include (ROOT.'/view/layouts/header.php');?>
<!-- <div class="menu-wrap"> -->
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li>
      <li><a href="#" >The Task</a></li>
      <li><a href="/administrativa"  class="active">Cabinet</a></li>
      <li style="background-color: black; margin-left: 49%; border-color: black;"><a href="/admin/logout">Logout</a></li>
    </ul>
  </div>
<!-- </div> -->

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>FIT<span>BANK SYSTEM</span></h1>
  </div>
  
</div>

<div class="page">
<a href="/administrativa"><img src="/template/images/newimages/back.png" class="backbutt"></a>
<br>
  <div class="generic">
    <div class="panel">
      <div class="title">
        <h1>ADD ACCOUNT</h1>
      </div>
      <hr>
      <div class="content">

        <?php if ( (isset($errors)) && (is_array($errors)) && (isset($_POST['submit'])) ): ?>
           <div class="error" >
            <br>
            <ul>
                <?php foreach ($errors as $error): ?>
                    <li> <h3><img src="/template/images/newimages/notok.png" class="notokimg">&nbsp<?php echo $error; ?></h3></li>
                <?php endforeach; ?>
            </ul>
        <?php endif;?>
        </div>
        
         <div class="noterror"><?php if(isset($complete) && isset($_POST['submit'])):?><img src="/template/images/newimages/ok.png" class="okimg">&nbsp OK CHANGED STATUS FOR ACC<br/> 
         <?php endif; ?>
         </div>
          <form action =""  method="POST" class="select">
             
              <br>Choose account from system:<br/>
              <select name="user">
                  <?php for($i = 0;$i<count($account);$i++): ?>
                  <option value = "<?php echo $account[$i]['cisloUctu']; ?>"> <?php echo $account[$i]['cisloUctu']."|".$account[$i]['stav']; ?> </option>
                  <?php endfor; ?>
                </select><br/>
            Choose account status:<br/>
            <select name="status">
                <option value="otevren">otevrit</option>
                <option value="uzavren">uzavrit</option>
            </select><br/>
           
            <input  value="submit" name="submit" type="submit">
        </form><br/><br/>
     </div>
    </div>
  </div>
</div>
<?php include (ROOT.'/view/layouts/footer.php');