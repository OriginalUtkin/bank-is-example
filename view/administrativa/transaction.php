<?php include (ROOT.'/view/layouts/header.php'); ?>
<div class="menu-wrap">
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li>
      <li><a href="#" >The Task</a></li>
      <li><a href="/cabinet"  class="active">Cabinet</a></li>
      <li style="background-color: black; margin-left: 49%; border-color: black;"><a href="/user/logout">Logout</a></li>
    </ul>
  </div>
</div>

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>FIT<span>BANK SYSTEM</span></h1>
  </div>
</div>

<div class="page"> 
<a href="/administrativa"><img src="/template/images/newimages/back.png" class="backbutt"></a>
<br>
<br>
<div class="statuscal">     
    <form method="POST" autocomplete="off" action="" class="formcal">
        <div class="cal"> from &nbsp<input type="text" name="od" class="tcal" placeholder="YYYY-MM-DD" value="" />&nbsp
         to &nbsp<input type="text" name="do" class="tcal" placeholder="YYYY-MM-DD" value="" /> &nbsp
         </div>
         <div class="cal"> <select class="turnintodropdown_demo2" name="status">
           <option>WAIT</option>
           <option>ALOWED</option>
           <option>DENIDE</option>
         </select>
         </div>
         &nbsp
        <input  value="OK" name="submit" type="submit" class="buttcal"><br/>
    </form>
  </div>

    <div class="generic transaction" >
        <div class="panel" style="width: 100%; display: inline-block;">
        <?php if(!empty($transakce)):?>
        <?php for($i = 0; $i < count($transakce);$i++ ): ?>
          <div style="float: left; margin-left: 3% ; margin-bottom: 20px; background-color: #E6E9F1;">
            <div class="title">
                <h1>TRANSACTION NUMBER:<?php echo $transakce[$i]['cisloTransakce']; ?> </h1>
            </div>
            <hr style="width: 75%; margin-left: 13%;">
            <br>
            <div class="content" style="height: 100px; text-align: left;padding: 10px;">
                operace:<?php echo $transakce[$i]['typ'];?><br/>
                <?php if($transakce[$i]['status']!='WAIT'): ?>
                provedl:<?php echo $transakce[$i]['cislo_zamestnance'];?>
                <?php endif; ?>
            </div>
          </div>
          <?php endfor; ?>
          <?php else: ?>
        </div>
    </div>

        <div class="generic transaction">
            <div class="panel">
                <div class="title">
                    <h1>NO TRANSACTIONS</h1>
                </div>
            </div>
        </div>
    <?php endif;?>
</div>
<?php include (ROOT.'/view/layouts/footer.php'); ?>