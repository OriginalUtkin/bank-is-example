<?php include ROOT.'/view/layouts/header.php';?>

<div class="menu-wrap">
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li><!-- Back to index page-->
      <li><a href="#" >The Task</a></li><!--Page with project task -->
      <li><a href="/administrativa"  class="active">Cabinet</a></li><!-- -->
      <li style="background-color: black; margin-left: 49%; border-color: black;"><a href="/user/logout">Logout</a></li>
    </ul>
  </div>
</div>
  

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>FIT<span>BANK SYSTEM</span></h1>
  </div>
</div>
<div class="page">
<a href="/administrativa"><img src="/template/images/newimages/back.png" class="backbutt"></a>
<br>
  <div class="generic">
    <div class="panel">
      <div class="titleinfo">
        <h1>PERSONAL DATA FOR <b><?php echo $admin['jmeno']." ".$admin['prijmeni'] ?></b></h1>
      </div>
      <hr>
      <br>
      <div class="contentinfo">
            <?php foreach ($admin as $key => $value): ?>
                <?php if($key != 'password'): ?>
                    <?php if($key === 'cisloZamestnance'): ?>
                        <h2 style="font-size: 18px;"><b><?php echo "Number in system: "?></b><?php echo $value ?></h2><br/>
                    <?php elseif ($key === 'rodne_cislo'):?>
                        <h2 style="font-size: 18px;"><b><?php echo "Rodne cislo is: "?></b><?php echo $value ?></h2><br/>                    
                    <?php else:?>
                        <?php if(empty($value)): ?>
                            <h2 style="font-size: 18px;"><b><?php echo $key.": ";?></b><span style="color: red;"><?php echo "Not defined"; ?></span></h2><br/>
                        <?php else: ?>
                            <h2 style="font-size: 18px;"><b><?php echo $key.": "?></b><?php echo $value; ?></h2><br>                                
                        <?php endif; ?>                                                        
                    <?php endif; ?>                    
                <?php endif; ?> 
            <?php endforeach; ?>
      </div>
    </div>
  </div>
</div>

<?php include ROOT.'/view/layouts/footer.php';
