<?php include ROOT.'/view/layouts/header.php';?>

<div class="menu-wrap">
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li><!-- Back to index page-->
      <li><a href="about.html" >The Task</a></li><!--Page with project task -->
      <li><a href="cabinet.html"  class="active">Cabinet</a></li><!-- -->
      <li><a href="/user/logout">Logout</a></li>
    </ul>
  </div>
</div>
  

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>FIT<span>BANK SYSTEM</span></h1>
  </div>
</div>

<div class="page">
  <div class="generic">
    <div class="panel">
      <div class="titleinfo">
        <h1>PERSONAL DATA FOR <b><?php echo $user['jmeno']." ".$user['prijmeni'] ?></b></h1>
      </div>
      <hr>
      <br>
      
      <div class="contentinfo">
            <?php foreach ($user as $key => $value): ?>
                <?php if($key != 'password'): ?>
                    <h2><?php echo $key.": ".$value; ?></h2><br>
                <?php endif; ?>                
            <?php endforeach; ?>
                <h2><?php echo $ucet['cisloUctu']; ?></h2>
      </div>


    </div>
  </div>
</div>

<?php include ROOT.'/view/layouts/footer.php';
