<?php include (ROOT.'/view/layouts/header.php'); ?>
<div class="menu-wrap">
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li>
      <li><a href="#" >The Task</a></li>
      <li><a href="/cabinet"  class="active">Cabinet</a></li>
      <li style="background-color: black; margin-left: 49%; border-color: black;"><a href="/user/logout">Logout</a></li>
    </ul>
  </div>
</div>

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>FIT<span>BANK SYSTEM</span></h1>
  </div>
</div>

<div class="page">
<a href="/cabinet"><img src="/template/images/newimages/back.png" class="backbutt"></a>
<br>
        <form method="POST" autocomplete="off" action="" class="formcal">
         from &nbsp<input type="text" name="od" class="tcal" placeholder="YYYY-MM-DD" value="" />&nbsp
         to &nbsp<input type="text" name="do" class="tcal" placeholder="YYYY-MM-DD" value="" /> &nbsp
        <input  value="OK" name="submit" type="submit" style="width: 100px;background-color: green;color: #ffffff;padding: 8px 24px 8px 10px;border: 1px solid;cursor: pointer;display: inline-block;"><br/>
    </form>

    <div class="generic transaction">
        <div class="panel" style="width: 100%; display: inline-block;">
          <?php if(!empty($transakce)):?>
          <?php for($i = 0; $i < count($transakce);$i++ ): ?>
          <div style="float: left; margin-left: 4%; margin-bottom: 20px; background-color: #E6E9F1;">
              <div class="title">
                <h1>TRANSACTION NUMBER:<?php echo $transakce[$i]['cisloTransakce']; ?></h1>
            </div>
            <hr style="width: 75%; margin-left: 13%;">
            <br>
            <div class="content" style="height: 220px; text-align: left;padding: 10px;">
                operace:&nbsp;<?php echo $transakce[$i]['typ'];?><br/>
                <?php $mena = Operations::getMena($transakce[$i]['cislo_uctu']) ?><br>
                castka:&nbsp;<?php echo $transakce[$i]['castka']." ".$mena['mena']; ?> <br/><br>
                date:&nbsp <?php echo $transakce[$i]['datum']; ?> <br/><br>
                from acc: &nbsp <span style="border: 1px solid #B1B6C4; padding: 5px; margin-bottom: 40px;"><?php echo $transakce[$i]['zUctu']; ?></span><br><br>
                to acc: &nbsp <span style="border: 1px solid #B1B6C4; margin-left: 20px; padding: 5px;"><?php echo $transakce[$i]['cislo_uctu']; ?>
                </span><br>
                <?php if($transakce[$i]['status']!='WAIT'): ?> <br>
                provedl:&nbsp;<?php echo $transakce[$i]['cislo_zamestnance'];?>
                <?php endif; ?>
            </div>
          </div>
          <?php endfor; ?>
          <?php else: ?>
        </div>
    </div>

        <div class="generic transaction">
            <div class="panel">
                <div class="title">
                    <h1>You have't transaction</h1>
                </div>
            </div>
        </div>
    <?php endif;?>
  <!--  <a href="<?php $_SERVER['HTTP_REFERER']; ?>">BACK</a> -->
</div>
<a href="/cabinet"><img src="/template/images/newimages/back.png" class="backbutt"></a>
<br>

<?php include (ROOT.'/view/layouts/footer.php'); ?>