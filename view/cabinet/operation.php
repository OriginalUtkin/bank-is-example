<?php include(ROOT.'/view/layouts/header.php'); ?>

<div class="menu-wrap">
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li><!-- Back to index page-->
      <li><a href="#" >The Task</a></li><!--Page with project task -->
      <li><a href="/cabinet" class="active">Cabinet</a></li>
      <li style="background-color: black; margin-left: 49%; border-color: black;"><a href="/user/logout">Logout</a></li>
     
    </ul>
  </div>
</div>

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>FIT<span>BANK SYSTEM</span></h1>
  </div>
  
</div>

<div class="page">
<a href="/cabinet"><img src="/template/images/newimages/back.png" class="backbutt"></a>
<br>
  <div class="generic">
    <div class="panel">
      <div class="title">
        <h1>New payment order</h1>
        
      </div>
      <hr>
      <br>
      <div class="content">
          <!--Если есть ошибка - вывести ошибку(и). Маркерованый список Одиночные поля списка <li> -->
        <?php if ( (!empty($errors)) && isset($_POST['submit']) ): ?>
          <div class="error" >
            <ul>
                <?php foreach ($errors as $error): ?>
                    <li> <h3><img src="/template/images/newimages/notok.png" class="notokimg"> <?php echo $error; ?></h3></li>
                <?php endforeach; ?>
            </ul>
        </div>
          <!-- Если все ок, то вывести сообщение об успешном проведении -->
        <div class="noterror"> 
        <?php elseif (empty($error) && isset($_POST['submit'])):?>
            <h3><img src="/template/images/newimages/ok.png" class="okimg">&nbsp<?php echo "Your payment order was successfully sent and has WAIT status"; ?></h3>
        <?php endif;?>
        </div>
        <br/>
        <br/>


      <div class="edit">  
        <form action ="" method="POST" class="formlog1">
            <span class="choose">Please, enter you account number<span><br/>
                <input  placeholder="*Your account here"   name="fromAcc" value="<?php if(isset($_SESSION['cabinet'.$value[0]])): ?><?php echo $_SESSION['cabinet'.$value[0]] ;endif; ?>" type="text"><br/>
            <span class="choose">Please, enter account number of recepient<span><br/>
                <input  placeholder="*Enemy account here"  name="toAcc"  value="<?php if(isset($_SESSION['cabinet'.$value[1]])): ?><?php echo $_SESSION['cabinet'.$value[1]] ;endif; ?>" type="text"><br/><br> 
            <span class="choose">Please, enter amount<span><br/>
                <input  placeholder="*Amount"  name="castka" value="<?php if(isset($_SESSION['cabinet'.$value[2]])): ?><?php echo $_SESSION['cabinet'.$value[2]] ;endif; ?>" type="text"><br/><br/>
            <input  value="SEND" name="submit" type="submit" style="width: 350px;height: 50px;margin-top: 20px;color: #ffffff;text-shadow: 0 1px 0 #133d3e;font-family: 'Arial, Geneva, sans-serif';font-size: 15pt;background:#1a202c;
  cursor: pointer;" class="buttomlog1">
        </form>  
      </div>     
      </div>
    </div>
  </div>
</div>




<?php include(ROOT.'/view/layouts/footer.php'); ?>

