<?php include ROOT.'/view/layouts/header.php';?>


<!-- <div class="menu-wrap"> -->
  
 <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li>
      <li><a href="#" >The Task</a></li>
      <li><a href="/cabinet"  class="active">Cabinet</a></li>
      <li style="background-color: black; margin-left: 49%; border-color: black;"><a href="/user/logout">Logout</a></li>
    </ul>
    <span></span>
  </div>
<!-- </div> -->
  

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>USER PANEL</h1>
  </div>
</div>

<div class="page">
  <div class="generic">
    <div class="panel">
      <div class="title">
        <h1><img src="/template/images/newimages/user.png" style="width: 40px; height: 40px;">&nbspPERSONAL AREA: <?php echo $user['jmeno']."  ".$user['prijmeni']?></h1>      
      </div>
      <hr>
      <div class="uscontent">
        <div class="uscab"><h2><a href="/cabinet/info">Personal info</a></h2><br/></div>
        <div class="uscab"><h2><a href="/cabinet/edit">Edit personal info</a></h2><br/></div>
        <div class="uscab"><h2><a href="/cabinet/operation">New payments</a></h2><br/></div>
        <div class="uscab"><h2><a href="/cabinet/transaction">Transactions history</a></h2><br/></div>
        <div class="uscab"><h2><a href="/cabinet/credits">Account info</a></h2><br/></div>       
      </div>
    </div>
  </div>
</div>

<?php include ROOT.'/view/layouts/footer.php';
