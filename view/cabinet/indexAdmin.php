<?php include ROOT.'/view/layouts/header.php';?>

<div class="menu-wrap">
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li>
      <li><a href="#" >The Task</a></li>
      <li><a href="/administrativa"  class="active">Cabinet</a></li>
      <li style="background-color: black; margin-left: 49%; border-color: black;"><a href="/admin/logout">Logout</a></li>
    </ul>
  </div>
</div>
  

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h2>ADMINISTRATION PANEL</h2>
  </div>

  
</div>



<div class="page">
  <div class="generic">
    <div class="panel1">
      <div class="title">
        <h1 style="text-align: center;"><img src="/template/images/newimages/admin.png" style="width: 40px; height: 40px;">&nbsp presonal area: employee</h1>
      </div>
      <hr>
      <div class="uscontent">
        <div class="uscab"><h2><a href="administrativa/info">Personal info</a></h2><br/></div>
        <div class="uscab"><h2><a href="/administrativa/transDo">Transactions control</a></h2><br/></div>
        <div class="uscab"><h2><a href="/administrativa/transaction">Transactions history</a></h2><br/></div>
        <div class="uscab"><h2><a href="/administrativa/addAccount">Add account to system</a></h2><br/></div>
        <div class="uscab"><h2><a href="/administrativa/addUser">Add user to system</a></h2><br/></div>
        <div class="uscab"><h2><a href="/administrativa/deleteUser">Remove user from system</a></h2><br/></div>
        <div class="uscab"><h2><a href="/administrativa/status">Change account status</a></h2><br/><div>
       
      </div>
    </div>
  </div>
</div>

<?php include ROOT.'/view/layouts/footer.php';
