<?php include ROOT.'/view/layouts/header.php';?>

<div class="menu-wrap">
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li><!-- Back to index page-->
      <li><a href="/administrativa"  class="active">Cabinet</a></li><!-- -->
      <li><a href="/user/logout">Logout</a></li>
    </ul>
  </div>
</div>
  

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>FIT<span>BANK SYSTEM</span></h1>
  </div>
</div>

<div class="page">
  <div class="generic">
    <div class="panel">
      <div class="titleinfo">
        <h1>PERSONAL DATA FOR <b><?php echo $admin['jmeno']." ".$admin['prijmeni'] ?></b></h1>
      </div>
      <hr>
      <br>

    </div>
  </div>
</div>

<?php include ROOT.'/view/layouts/footer.php';
