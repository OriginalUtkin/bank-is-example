<?php include (ROOT.'/view/layouts/header.php');?>
<div class="menu-wrap">
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li>
      <li><a href="about.html" >The Task</a></li>
      <li><a href="/aIndex"  class="active">Cabinet</a></li>
      <li style="background-color: black; margin-left: 47%; border-color: black;"><a href="administration/logout">Logout</a></li>
    </ul>
  </div>
</div>

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>FIT<span>BANK SYSTEM</span></h1>
  </div>
  
</div>

<div class="page">
<a href="/aIndex"><img src="/template/images/newimages/back.png" class="backbutt"></a>
<br>
  <div class="generic">
    <div class="panel">
      <div class="title">
        <h1>EDIT BALANCE FOT ACCOUNT</h1>
      </div>
      <hr>
      <div class="content">
              <form action =""  method="POST">
              <div class="noterror"><?php if($complete == TRUE):?> 
              <img src="/template/images/newimages/ok.png" class="okimg"><?php echo"Balance changed";  ?>
              <?php endif; ?></div><br/>
              <?php if ( (isset($errors)) && (is_array($errors)) && isset($_POST['submit']) ): ?>
          <div class="error" >
          <ul>
            <?php foreach ($errors as $error): ?>
            <li> <h3><img src="/template/images/newimages/notok.png" class="notokimg">&nbsp<?php echo $error; ?></h3></li>
          <?php endforeach; ?>
            
          </ul>
        <?php endif;?>
              
              Choose account from system<br/><br/>
              <select name="account">
                  <?php for($i = 0; $i < count($accounts); $i++): ?>
                    <option value = "<?php echo $accounts[$i]['cisloUctu']; ?>"> <?php echo $accounts[$i]['cisloUctu']." "."|"." ".$accounts[$i]['stav']; ?> </option>
                  <?php endfor; ?>
                </select><br/><br/>
                <input style="width: 160px;border: 1px solid;background-color: #1a202c;padding: 8px 24px 8px 10px;letter-spacing: .075em;color: #ffffff;text-shadow: 0 1px 0 rgba(0,0,0,.1);margin-bottom: 19px;" type="text" placeholder="Add to balance" name ="balance"><br/><br/>
            <input style="width: 140px; height: 30px; background: #202431; color: #ffffff; border-top-color: #9fb5b5;border-left-color: #608586;border-bottom-color: #1b4849;border-right-color: #1e4d4e;" value="ADD" name="submit" type="submit">
        </form><br/><br/>

     </div>
    </div>
  </div>
</div>
<?php include (ROOT.'/view/layouts/footer.php');