 <?php include (ROOT.'/view/layouts/header.php');?>
<div class="menu-wrap">
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li>
      <li><a href="#" >The Task</a></li>
      <li><a href="/aIndex"  class="active">Cabinet</a></li>
      <li style="background-color: black; margin-left: 47%; border-color: black;"><a href="/administration/logout">Logout</a></li>
    </ul>
  </div>
</div>


<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>FIT<span>BANK SYSTEM</span></h1>
  </div>
  
</div>
    

<div class="page">
<a href="/aIndex"><img src="/template/images/newimages/back.png" class="backbutt"></a>
<br>
  <div class="generic">
    <div class="panel">
      <div class="title">
        <h1>ADD USER TO SYSTEM</h1>
      </div>
      <hr>
      <div class="content">
      <div class="noterror"> 

       <!-- есть ошибки -->
      <?php if ( (isset($errors)) && (is_array($errors)) && isset($_POST['submit']) ): ?>
          <div class="error" >
          <ul>
            <?php foreach ($errors as $error): ?>
            <li> <h3><img src="/template/images/newimages/notok.png" class="notokimg">&nbsp<?php echo $error; ?></h3></li>
          <?php endforeach; ?>
            
          </ul>
        <?php endif;?>
        
        </div>
          <!-- НЕТ ОШИБКИ -->
          <div class="noterror"><?php if($complete): ?>
          <img src="/template/images/newimages/ok.png" class="okimg"><?php echo "User add to system"; ?>
          <?php endif;?>
        </div><br><br>


        <form action ="" method="POST" class="editdata">
            Enter first name:<br/>
            <input  placeholder="First name" required  name="name" value="<?php if(isset($_SESSION['u'.$value[0]])): ?><?php  echo $_SESSION['u'.$value[0]]; endif;?>" type="text" ><br/>
            Enter second name:<br/>
            <input  placeholder="Second name" required=""  name="surname" value="<?php if(isset($_SESSION['u'.$value[1]])): ?><?php  echo $_SESSION['u'.$value[1]]; endif;?>" type="text"><br/>
            Enter street:<br/>
            <input  placeholder="Street"   name="street" value="<?php if(isset($_SESSION['u'.$value[2]])): ?><?php  echo $_SESSION['u'.$value[2]]; endif;?>" type="text"><br/>
            Enter town:<br/>
            <input  placeholder="Town"  name="town" value="<?php if(isset($_SESSION['u'.$value[3]])): ?><?php  echo $_SESSION['u'.$value[3]]; endif;?>" type="text"><br/>
            Enter email:<br/>
            <input  placeholder="E-mail" required="" name="email" value="<?php if(isset($_SESSION['u'.$value[4]])): ?><?php  echo $_SESSION['u'.$value[4]]; endif;?>" type="text"><br/>
            Enter psc:<br/>
            <input  placeholder="psc"   name="psc" value="<?php if(isset($_SESSION['u'.$value[5]])): ?><?php  echo $_SESSION['u'.$value[5]]; endif;?>" type="text"><br/>
            Enter password:<br/>
            <input  placeholder="Password" required="" name="password" value="<?php if(isset($_SESSION['u'.$value[6]])): ?><?php  echo $_SESSION['u'.$value[6]]; endif;?>" type="text"><br/>
            Enter telefon: <br/>
            <input  placeholder="Telefon"   name="phone" value="<?php if(isset($_SESSION['u'.$value[7]])): ?><?php  echo $_SESSION['u'.$value[7]]; endif;?>" type="text"><br/>
            Enter rodne_cislo:<br/>
            <input  placeholder="Rodne cislo" required="" name="rc" value="<?php if(isset($_SESSION['u'.$value[8]])): ?><?php  echo $_SESSION['u'.$value[8]]; endif;?>" type="text"><br/>
         <br/>
        <input  value="Add" name="submit" type="submit" class="editbutt">
        </form><br/><br/>

        
       
      </div>
    </div>
  </div>
</div>
<?php include (ROOT.'/view/layouts/footer.php');?>