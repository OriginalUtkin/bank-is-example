<?php include ROOT.'/view/layouts/header.php';?>

<!-- <div class="menu-wrap"> -->
  <div class="menu">
    <ul>
      <li><a href="/" >Home</a></li>
      <li><a href="#" >The Task</a></li>
      <li><a href="/aIndex"  class="active">Cabinet</a></li>
      <li style="background-color: black; margin-left: 47%; border-color: black;"><a href="/administration/logout">Logout</a></li>
    </ul>
  </div>
<!-- </div> -->
  

<div class="clearing"></div>
<div class="header">
  <div class="logo">
    <h1>ADMINISTRATOR PANEL</h1>
  </div>
</div>

<div class="page">
  <div class="generic">
    <div class="panel">
      <div class="title">
        <h1><img src="/template/images/newimages/admin.png" style="width: 40px; height: 40px;">&nbsp PERSONAL AREA: <?php echo $administrator['jmeno']."  ".$administrator['prijmeni']?></h1>
      </div>
      <hr>
        
      <div class="uscontent1">
        <div class="uscab1"><h2><a href="addPracovnik">Add employee to system</a></h2><br/></div>
        <div class="uscab1"><h2><a href="deletePracovnik">Delete employee from system</a></h2><br/></div>
        <div class="uscab1"><h2><a href="addU">Add user to system</a></h2><br/></div>
        <div class="uscab1"><h2><a href="deleteU">Delete user from system</a></h2><br/></div>
         <div class="uscab1"><h2><a href="balanceAdd">Edit balance</a></h2><br/></div>
        
      </div>
    </div>
  </div>
</div>

<?php include ROOT.'/view/layouts/footer.php';
