<?php

class Operations {
    
    public static $sql;
    
     public function checkAccount($id,$accNo){
         $db = Db::getConnection();
         $sql = 'SELECT * FROM ucet WHERE id_klient = :id AND cisloUctu = :accNo';
         $result = $db->prepare($sql);
         $result->bindParam(':id', $id,PDO::PARAM_INT);
         $result->bindParam(':accNo', $accNo,PDO::PARAM_STR);
         $result->setFetchMode(PDO::FETCH_ASSOC);
         $result->execute();
         
         $existAcc = $result->fetch();
         
         if(empty($existAcc)){
             return false;
         }
       
         else{
             return true;
         }
    }
    
    public function checkTargetAccount($accNo){
        $db = Db::getConnection();
        $status = 'otevren';
        $sql = "SELECT * FROM ucet WHERE cisloUctu = :accNo AND stav = :status";
        $result = $db->prepare($sql);
        $result->bindParam(':accNo', $accNo,PDO::PARAM_STR);
        $result->bindParam(':status', $status,PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        
        $existAcc = $result->fetch();
        
        if(empty($existAcc)){
            return false;
        }
        else{
            return true;
        }
    }
   
    public function checkBalance($accNo,$sum){
        $db = Db::getConnection();
        $sql = 'SELECT dostupna_castka FROM disponuje WHERE cislo_uctu=:accNo';
        $result = $db->prepare($sql);
        $result->bindParam(':accNo',$accNo,PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $balance = $result->fetch();
        $detekce = $balance['dostupna_castka'] - $sum;
        if($detekce >= 0){
            return true;
        }
        else{
         return false;   
        }
    }
    
    public function checkLimit($accNo, $sum){
        $db = Db::getConnection();
        $sql = 'SELECT limit_uctu, dostupna_castka FROM disponuje WHERE cislo_uctu = :accNo';
        $result = $db->prepare($sql);
        $result->bindParam(':accNo',$accNo,PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $limit = $result->fetch();
        $detekce = $limit['dostupna_castka'] + $sum;
        if($detekce < $limit['limit_uctu']){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function addTransaction($castka,$cislo_klienta,$zUctu,$cisloUctu){
        $date = date("Y-m-d");
        $transactionStatus = 'WAIT';
        $transactionTyp = 'prevod';
   
        $db = Db::getConnection();
        $sql = "INSERT INTO transakce(typ,castka,datum,cislo_klienta,zUctu,cislo_uctu,cislo_zamestnance,status) VALUES(:typ,:castka,'$date',:cislo_klienta,:zUctu,:cisloUctu,1,:status);";
        
        $result = $db->prepare($sql);
        $result->bindParam(':typ', $transactionTyp,PDO::PARAM_STR);
        $result->bindParam(':castka', $castka,PDO::PARAM_INT);
        $result->bindParam(':cislo_klienta', $cislo_klienta,PDO::PARAM_INT);
        $result->bindParam(':zUctu', $zUctu,PDO::PARAM_STR);
        $result->bindParam(':cisloUctu', $cisloUctu,PDO::PARAM_STR);
        $result->bindParam(':status', $transactionStatus,PDO::PARAM_STR);
    
       $result ->execute();
    }

    public function checkMena($to, $from){
        $to_mena = self::getMena($to);
        $from_mena = self::getMena($from);
        //echo $to_mena['mena'];
        $cmp_array = array($to_mena['mena'],$from_mena['mena']);
        $final_array = array_unique($cmp_array);
        if(count($final_array) === 1){
            return true;
        }else{
            return false;
        }
    }
    
    public function getMena($acc){
        $db = Db::getConnection();
        self::$sql = 'SELECT mena FROM ucet WHERE cisloUctu = :acc';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':acc', $acc, PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $final = $result->fetch();
        return($final) ;
    }
    
}
