<?php

class Administrator {
    public static $sql ;    
    public function checkData($email, $password){
        self::$sql = 'SELECT * FROM administrator WHERE email = :email AND password = :password';
        
        $db = Db::getConnection();
        $result = $db->prepare(self::$sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        
        $final = $result->fetch();
        
        if(empty($final)){
            return FALSE;
        }
        else{
            return $final['id'];
        }
    }
    
    public function auth($id){
        session_start();
        $_SESSION['administrator'] = $id; 
    }
    
    public function checkLogged(){
        session_start();
        if(isset($_SESSION['administrator'])){
            return $_SESSION['administrator'];
        }
        else{
            header("Location: /");
        }
    }
    
    public function isGuest(){
        if(isset($_SESSION['administrator'])){
            return false;
        }
        else{
            return true;
        }
    }

    public function getPracovnik(){
        $db = Db::getConnection();
        self::$sql = 'SELECT cisloZamestnance, jmeno,prijmeni FROM pracovnik';
        $result = $db->prepare(self::$sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        
        return ( $result->fetchAll() );
    }
    
    public function  deletePracovnik($id){
        $db = Db::getConnection();
        self::$sql = 'DELETE FROM pracovnik WHERE cisloZamestnance = :id';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
    }
    
     public function  deleteUser($id){
        $db = Db::getConnection();
        self::$sql = 'DELETE FROM klient WHERE cisloKlienta = :id';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
    }
    
    public function tableColumns(){
        $columns = array("klient","pracovnik","ucet","all");
        return $columns;
    }

    public function deleteTable($table){
        $db = Db::getConnection();

        if($table != 'all'){
            if($table === 'klient'){
                self::$sql = 'TRUNCATE TABLE klient';
                echo "DELITED KLIENT";
            }
            
            else if($table === 'pracovnik'){
                self::$sql = 'TRUNCATE TABLE pracovnik';
                echo "DELITED PRACOVNIK";
            }
            
            else if($table === 'ucet'){
                self::$sql = 'TRUNCATE TABLE ucet';
                echo "DELITED UCET";
            }
            
            $result = $db->prepare(self::$sql);
            $result->execute();
        }
        else{
            $colums = self::tableColumns();
            array_pop($colums);
            foreach($colums as $value){
                self::$sql = 'TRUNCATE TABLE :name';
                $result = $db->prepare(self::$sql);
                $result->bindParam(':name', $value, PDO::PARAM_STR);
                $result->execute();
            }
            echo "DELITED ALL";
        }
    }
    
    public function addPracovnik($name, $surname, $street, $town, $email, $psc, $password, $phone, $rc){
        $db = Db::getConnection();
        self::$sql = 'INSERT INTO pracovnik(jmeno,prijmeni,ulice,mesto,email,psc,password,telefon,rodne_cislo) '
                . 'VALUES(:name,:surname,:street,:town,:email,:psc,:password,:phone,:rc)';
        $result= $db->prepare(self::$sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':surname', $surname, PDO::PARAM_STR);
        $result->bindParam(':street', $street, PDO::PARAM_STR);
        $result->bindParam(':town', $town, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':psc', $psc, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->bindParam(':phone', $phone, PDO::PARAM_STR);
        $result->bindParam(':rc',$rc,PDO::PARAM_INT);
        $result->execute();
    }
    
    public function checkEmail($email){
        
         if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return ('WRONG_FORMAT');
         }
        
        $db = Db::getConnection();
        self::$sql = 'SELECT * FROM pracovnik WHERE email = :email';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':email', $email,PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $test = $result->fetch();
        
        if(!empty($test)){
            return ('ALREADY_EXIST');
        }
        return OK;
    }
    
    public function checkRc($rc){
        $db = Db::getConnection();
        self::$sql = 'SELECT * FROM pracovnik WHERE rc = :rc';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':rc', $rc,PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $final = $result->fetch();
        
        if(empty($final)){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
    public function deleteAccount($accNo){
        $db = Db::getConnection();
        self::$sql = 'DELETE FROM ucet WHERE cisloUctu = :accNo';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':accNo', $accNo, PDO::PARAM_STR);
        $result->execute();
    }

    public function changeBalance($accNo,$add){
        $db = Db::getConnection();
        self::$sql = 'SELECT dostupna_castka,limit_uctu FROM disponuje WHERE cislo_uctu = :accNo';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':accNo', $accNo,PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $balance =  $result->fetch();
        $lenght = strlen($add);
                
        if(preg_match_all( "/[0-9]/", $add,$out)!=$lenght){
            return false;
        }
        
        $new_balance = $balance['dostupna_castka'] + $add;
        
        if($new_balance > $balance['limit_uctu']){
            return false;
        }else{
        self::$sql = 'UPDATE disponuje SET dostupna_castka = :newCastka WHERE cislo_uctu = :accNo';
        $result1 = $db->prepare(self::$sql);
        $result1->bindParam(':newCastka', $new_balance, PDO::PARAM_INT);
        $result1->bindParam('accNo', $accNo, PDO::PARAM_STR);
        $result1->execute();
        return true;
        }
    }
}
