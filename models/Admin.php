<?php

class Admin {
    public static $sql;
    public static $result;
    
    public function checkAdminData($email,$password){
        $db = Db::getConnection();
        self::$sql = 'SELECT * FROM pracovnik WHERE email = :email AND password = :password';
        self::$result = $db->prepare(self::$sql);
        self::$result->bindParam(':email', $email, PDO::PARAM_STR);
        self::$result->bindParam(':password',$password,PDO::PARAM_STR);
        self::$result->setFetchMode(PDO::FETCH_ASSOC);
        self::$result->execute();
        
        $admin = self::$result->fetch();
        
         if($admin){
            return $admin['cisloZamestnance'];
        }
        else
        {
            return false;
        }
    }
    
    public function auth($adminId){
        session_start();
        $_SESSION['admin'] = $adminId;
        
        $lastVisit = time();
        $db = Db::getConnection();
        
        $sql = "INSERT INTO statusp VALUES($adminId,$lastVisit)";
        $result = $db->prepare($sql);
        $result->execute();
    }
    
    public static function checkLogged(){
        session_start();
        if(isset($_SESSION['admin'])){
            return $_SESSION['admin'];
        }
        else{
            header("Location: /admin/login");
        }
    }
    
    public static function getAdminByID($id){
       
        $db = Db::getConnection();
        self::$sql = 'SELECT * FROM pracovnik where cisloZamestnance = :id';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        
        return $result->fetch();
    }
    
       public static function isGuest(){        
        if(isset($_SESSION['admin'])){
            return false;
        }
        return true;
    }
    
    public function getData($id){
        $db = Db::getConnection();
        self::$sql = 'SELECT * FROM pracovnik WHERE cisloZamestnance = :id';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':id', $id, PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return ($result->fetch());
    }

    public static function getTransaction(){
        $status = 'WAIT';
        $db = Db::getConnection();
        self::$sql = 'SELECT * FROM transakce WHERE status = :status';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':status', $status,PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        
        return $result->fetchAll();
    }
   

    public static function getAccounts(){
        $db = Db::getConnection();
        self::$sql = 'SELECT cisloUctu,stav FROM ucet';
        $result = $db->prepare(self::$sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        
        return $result->fetchAll();
    }
    
    public static function changeStatus($accNo,$stav){
        $db = Db::getConnection();
        self::$sql = 'SELECT id_klient FROM ucet WHERE cisloUctu = :accNo AND stav = :stav';
        $result1 = $db->prepare(self::$sql);
        $result1->bindParam(':accNo', $accNo,PDO::PARAM_STR);
        $result1->bindParam(':stav', $stav,PDO::PARAM_STR);
        
        $result1->execute();
        $test = $result1->fetch();
        if(!empty($test)){
            return false;
        }else{
            self::$sql = 'UPDATE ucet SET stav = :stav WHERE cisloUctu = :accNo';
            $result = $db->prepare(self::$sql);
            $result->bindParam(':stav', $stav,PDO::PARAM_STR);
            $result->bindParam(':accNo', $accNo,PDO::PARAM_STR);
            $result->execute();
            return true;
        }
    }


    public static function acceptTransaction($id,$adminId){
        $db = Db::getConnection();
        self::$sql = 'SELECT castka,zUctu,cislo_uctu,cislo_zamestnance FROM transakce WHERE cisloTransakce = :id';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        
        $transakce = $result->fetch();
        
        self::$sql = 'SELECT dostupna_castka FROM disponuje WHERE cislo_uctu = :accNo';
        $result1= $db->prepare(self::$sql);
        $result1->bindParam(':accNo', $transakce['zUctu'],PDO::PARAM_STR);
        $result1->setFetchMode(PDO::FETCH_ASSOC);
        $result1->execute();

        $fromAcc = $result1->fetch();
                
        self::$sql = 'SELECT dostupna_castka FROM disponuje WHERE cislo_uctu = :accNo';
        $result2= $db->prepare(self::$sql);
        $result2->bindParam(':accNo', $transakce['cislo_uctu'],PDO::PARAM_STR);
        $result2->setFetchMode(PDO::FETCH_ASSOC);
        $result2->execute();
  
        $toAcc = $result2->fetch();

        $fromAcc['dostupna_castka'] = $fromAcc['dostupna_castka'] - $transakce['castka'];
        $toAcc['dostupna_castka'] = $toAcc['dostupna_castka'] + $transakce['castka'];
        
        self::$sql = 'UPDATE disponuje SET dostupna_castka = :balance WHERE cislo_uctu = :accNo ';
        $result3 = $db->prepare(self::$sql);
        $result3 ->bindParam(':balance', $fromAcc['dostupna_castka'], PDO::PARAM_INT);
        $result3 ->bindParam(':accNo', $transakce['zUctu'], PDO::PARAM_STR);
        $result3->execute();
        
        self::$sql = 'UPDATE disponuje SET dostupna_castka = :balance WHERE cislo_uctu = :accNo';
        $result4 = $db->prepare(self::$sql);
        $result4 ->bindParam(':balance', $toAcc['dostupna_castka'], PDO::PARAM_INT);
        $result4 ->bindParam(':accNo', $transakce['cislo_uctu'], PDO::PARAM_STR);
        $result4->execute();

        $status = 'ALOWED';
        self::$sql = 'UPDATE transakce SET status =:status WHERE cisloTransakce =:id';
        $result5 = $db->prepare(self::$sql);
        $result5->bindParam(':id', $id,PDO::PARAM_INT);
        $result5->bindParam(':status', $status, PDO::PARAM_STR);
        $result5->execute();
        
        if($adminId != $transakce['cislo_zamestnance']){
            self::$sql = 'UPDATE transakce SET cislo_zamestnance = :idZ WHERE cisloTransakce = :id';
            $result6 = $db->prepare(self::$sql);
            $result6->bindParam(':idZ', $adminId, PDO::PARAM_INT);
            $result6->bindParam(':id', $id, PDO::PARAM_INT);
            $result6->execute();
        }
    }
    
    public static function declineTransaction($id,$adminId){
        $db = Db::getConnection();
        
        
        $status = 'DENIDE';
        self::$sql = 'UPDATE transakce SET status =:status WHERE cisloTransakce =:id';
        $result5 = $db->prepare(self::$sql);
        $result5->bindParam(':id', $id,PDO::PARAM_INT);
        $result5->bindParam(':status', $status, PDO::PARAM_STR);
        $result5->execute();
        
        self::$sql = 'UPDATE transakce SET cislo_zamestnance = :idZ WHERE cisloTransakce = :id';
        $result6 = $db->prepare(self::$sql);
        $result6->bindParam(':idZ', $adminId, PDO::PARAM_INT);
        $result6->bindParam(':id', $id, PDO::PARAM_INT);
        $result6->execute();
    }

    public static function addUcet($number, $mena, $limit, $id){
        $db = Db::getConnection();
        $date =date("Y-m-d");
        $stav = 'otevren';
        $balance = 0;
        
        self::$sql ="INSERT INTO ucet(cisloUctu,datumZrizeni,stav,mena,id_klient) VALUES(:number,'$date','$stav',:mena,:id)";
        $result = $db->prepare(self::$sql);
        $result->bindParam(':number', $number, PDO::PARAM_STR);
        $result->bindParam(':mena', $mena, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        
        self::$sql = 'INSERT INTO disponuje (dostupna_castka,limit_uctu,cislo_klienta,cislo_uctu) VALUES(:balance,:limit,:id,:accNo)';
        $result1 = $db->prepare(self::$sql);
        $result1->bindParam(':balance', $balance, PDO::PARAM_INT);
        $result1->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result1->bindParam(':id', $id,PDO::PARAM_INT);
        $result1->bindParam(':accNo', $number,PDO::PARAM_STR);
        $result1->execute();
        
        return true;
    }
    
    public static function checkLimit($limit){
        $lenght = strlen($limit);
        if($lenght > 11){
            return FALSE;
        }
        if(preg_match_all("/[0-9]/",$limit,$out)!= $lenght){
            return FALSE;
        }
        return true;
    }
    
    public static function checkAccount($account){
        $prefix = substr($account,0,2);
        $postfix = substr($account,2,22);
        $lenght = strlen($account);
        
        if($lenght != 24 ){
            return ('LENGHT_ERROR');
        }
        
        if(strcmp($prefix, 'CZ')!=0){
            if(strcmp($prefix, 'EU')!=0){
                if(strcmp($prefix, 'US')!=0){
                    return("PREFIX_ERROR");
                }
            }
        }
        if(preg_match_all( "/[0-9]/", $postfix,$out) != 22){
            return ('POSTFIX_ERROR');
        }
        if(!self::checkAccountExist($account)){
            return ('ALREADY_EXIST');
        }
        return true;
    }
    
    public static function checkAccountExist($accNo){
         $db = Db::getConnection();
         $sql = 'SELECT * FROM ucet WHERE cisloUctu = :accNo';
         $result = $db->prepare($sql);
         
         $result->bindParam(':accNo', $accNo,PDO::PARAM_STR);
         $result->setFetchMode(PDO::FETCH_ASSOC);
         $result->execute();
         $existAcc = $result->fetch();
         if(empty($existAcc)){
             return TRUE;
         }
         else{
             return FALSE;
         }
    }
    
    public static function formTest($string,$defaultLenght){
        $lenght = strlen($string);
        if(preg_match_all("/[0-9]/",$string,$out)!= $lenght){
            return FALSE;
        }
        
        if($lenght != $defaultLenght){
            return FALSE;
        }
        return TRUE;
    }
    
    public static function addUser($name,$surname,$street,$town,$email,$psc,$password,$phone,$rodne_cislo){
        $db = Db::getConnection();
        self::$sql = 'INSERT INTO klient(jmeno,prijmeni,ulice,mesto,email,psc,password,telefon,rodne_cislo) '
                . 'VALUES(:name,:surname,:street,:town,:email,:psc,:password,:phone,:rc)';
        $result= $db->prepare(self::$sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':surname', $surname, PDO::PARAM_STR);
        $result->bindParam(':street', $street, PDO::PARAM_STR);
        $result->bindParam(':town', $town, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':psc', $psc, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->bindParam(':phone', $phone, PDO::PARAM_STR);
        $result->bindParam(':rc',$rodne_cislo,PDO::PARAM_INT);
        $result->execute();
    }
    public static function logout(){
        session_start();
        unset($_SESSION["admin"]);
        header("Location: /");
    }

    
    
    public static function checkOnline($userId){
        $db = Db::getConnection();
        self::$sql = 'SELECT * FROM statusp WHERE id = :id';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':id', $userId, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $status = $result->fetch();
        
        if(empty($status)){
            self::logout();
        }
        $now = time();
        $newTime = $now - $status['time'];
        if( $newTime < 600){
            self::$sql = "UPDATE statusp SET time = $now WHERE id = $userId ";
            $result1  = $db->prepare(self::$sql);
            $result1->execute();
        }
        else{
              self::$sql = "DELETE FROM statusp WHERE id = $userId ";
              $result2 = $db->prepare(self::$sql);
              $result2->execute();
              self::logout();
        }
    }
    
    public function getStatus(){
        $db = Db::getConnection();
        self::$sql = 'SELECT * FROM statusp';
        $result = $db->prepare(self::$sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
    }
    
    public function checkRc($rc,$table){
        $db = Db::getConnection();
        self::$sql = "SELECT * FROM $table WHERE rodne_cislo = :rc";
        $result = $db->prepare(self::$sql);
        $result->bindParam(':rc', $rc,PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $final = $result->fetch();
        
        if(empty($final)){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
    public static function dateTest($od,$do,$status){
        $db = Db::getConnection();
        
        if((isset($od)) && ($do === 0)){
            self::$sql = "SELECT * FROM transakce WHERE datum >= '$od' AND status = '$status'";
            $result = $db->prepare(self::$sql);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();
           return $result->fetchAll();
            
        }
        else if((isset($do)) && ($od === 0)){
            self::$sql = "SELECT * FROM transakce WHERE datum <= '$do' AND status = '$status'";
            $result = $db->prepare(self::$sql);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();
            return $result->fetchAll();
        }
        else{
            self::$sql = "SELECT * FROM transakce WHERE '$od' < datum  AND datum < '$do' AND status = '$status'";
            $result = $db->prepare(self::$sql);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();
            return $result->fetchAll();
        }
    }
    
    public function getAllTransaction($status){
            $db = Db::getConnection();
            self::$sql = "SELECT * FROM transakce WHERE status = '$status'";
            $result = $db->prepare(self::$sql);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();
            return $result->fetchAll();
    }
    
    public function getTransStatus($id){
        $db = Db::getConnection();
        self::$sql = "SELECT status FROM transakce WHERE cisloTransakce = :id";
        $result = $db->prepare(self::$sql);
        $result->bindParam(':id', $id);
        $result ->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $status = $result->fetch();
        
        if($status['status'] === 'ALOWED' || $status['status']==='DENIDE'){
            return true;
        }else{
            return false;
        }
    }
    
}
