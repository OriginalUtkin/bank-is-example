<?php

class User{
    public static $sql;
    public static $result;
  
    public  function checkUserData($email, $password){
        $db = Db::getConnection();
        $sql = 'SELECT * FROM klient WHERE email = :email AND password = :password ';
        
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email,PDO::PARAM_STR);
        $result->bindParam(':password', $password,PDO::PARAM_STR);
        $result->execute();
        
        $user = $result->fetch();
        
        if($user){
            return $user['cisloKlienta'];
        }
        return false;
    }
    
    public  function auth($userId){
        session_start();
        $_SESSION['user'] = $userId;
        
        $lastVisit = time();
        $db = Db::getConnection();
        
        $sql = "INSERT INTO status VALUES($userId,$lastVisit)";
        $result = $db->prepare($sql);
        $result->execute();
    }
    
    public  function checkLogged(){
        session_start();
        if(isset($_SESSION['user'])){
            return $_SESSION['user'];
        }
        else{
            header("Location: /user/login");
        }
    }
    
    public  function isGuest(){
        
        if(isset($_SESSION['user'])){
            return false;
        }
        return true;
    }
    public function getUserEmail($acc){
        $db = Db::getConnection();
        self::$sql = 'SELECT id_klient FROM ucet WHERE cisloUctu = :accNo ';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':accNo', $acc,PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $final = $result->fetch();
        
        self::$sql = 'SELECT email FROM klient WHERE cisloKlienta = :final ';
        $result1 = $db->prepare(self::$sql);
        $result1->bindParam(':final', $final,PDO::PARAM_INT);
        $result1->setFetchMode(PDO::FETCH_ASSOC);
        $result1->execute();
        
        return( $result1->fetch() );
    }

    public function getAllBalance($id){
        $db = Db::getConnection();
        self::$sql = 'SELECT dostupna_castka FROM disponuje WHERE cislo_klienta =:id';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':id', $id,PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $final = $result->fetchAll();
        
        $tmp = 0;
        for($i = 0 ; $i < count($final);$i++){
            $tmp +=$final[$i]['dostupna_castka'];
        }
        return $tmp;
     }

        public  function getUserByID($id){
        $db = Db::getConnection();
        $sql = 'SELECT * FROM klient where cisloKlienta = :id';
        
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        
        return $result->fetch();
    }
 
    public  function getUserUcetByID($id) {
        
        $db = Db::getConnection();
        $sql = 'SELECT * FROM disponuje where cislo_klienta = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id',$id,PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
          
        return $result->fetchAll();
    }
    
    public  function getTransaction($id){
        $db = Db::getConnection();
        $sql = 'SELECT * FROM transakce WHERE cislo_klienta = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id,PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        
        return $result->fetchAll();
    }
    
    public  function dateTest($od,$do,$id){
        $db = Db::getConnection();
        if((isset($od)) && ($do === 0)){
            self::$sql = "SELECT * FROM transakce WHERE datum >= '$od' AND cislo_klienta = $id";
            $result = $db->prepare(self::$sql);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();
           return $result->fetchAll();
            
        }
        else if((isset($do)) && ($od === 0)){
            self::$sql = "SELECT * FROM transakce WHERE datum <= '$do' AND cislo_klienta = $id";
            $result = $db->prepare(self::$sql);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();
            return $result->fetchAll();
        }
        else{
            self::$sql = "SELECT * FROM transakce WHERE '$od' < datum  AND datum < '$do' AND cislo_klienta = $id";
            $result = $db->prepare(self::$sql);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();
            return $result->fetchAll();
        }
    }
    
    public  function checkEmail($email){
        
         if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return ('WRONG_FORMAT');
         }
        
        $db = Db::getConnection();
        self::$sql = 'SELECT * FROM klient WHERE email = :email';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':email', $email,PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $test = $result->fetch();
        
        if(!empty($test)){
            return ('ALREADY_EXIST');
        }
        return OK;
    }

    public  function editEmail($id, $email){
        $db = Db::getConnection();
        
        self::$sql = 'UPDATE klient SET email = :email WHERE cisloKlienta = :id';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        
    }
    
    public  function edit($id,$columnName,$string){
        $db = Db::getConnection();
        
        if($columnName === 'ulice'){
            self::$sql = "UPDATE klient SET $columnName = '$string' WHERE cisloKlienta = :id";
        }
        
        if($columnName === 'mesto'){
            self::$sql ="UPDATE klient SET $columnName = '$string' WHERE cisloKlienta = :id";
        }
        if($columnName === 'telefon'){
            self::$sql = "UPDATE klient SET $columnName = $string WHERE cisloKlienta = :id";
        }
        
        
        $result = $db->prepare(self::$sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
    }
    

    public  function editPassword($id, $password){
        $db = Db::getConnection();
        
        self::$sql = 'UPDATE klient SET password = :password WHERE cisloKlienta = :id';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
    }
    
    public  function checkPassword($password){
        $lenght = mb_strlen($password);
        
        if($lenght < 6){
            return 'LESS_PASSWORD';
        }
        else{
            return 'OK';
        }
    }
    
    public  function getUsers(){
        $db = Db::getConnection();
        self::$sql = 'SELECT cisloKlienta,prijmeni,email,rodne_cislo FROM klient';
        $result = $db->prepare(self::$sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetchAll();
    }
    
    public function logout(){
        session_start();
        unset($_SESSION["user"]);
        header("Location: /");
    }

    public function checkOnline($userId){
        $db = Db::getConnection();
        self::$sql = 'SELECT * FROM status WHERE id = :id';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':id', $userId, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        $status = $result->fetch();
        
        if(empty($status)){
            self::logout();
        }
        $now = time();
        $newTime = $now - $status['time'];
        if( $newTime < 600){
            self::$sql = "UPDATE status SET time = $now WHERE id = $userId ";
            $result1  = $db->prepare(self::$sql);
            $result1->execute();
        }
        else{
              self::$sql = "DELETE FROM status WHERE id = $userId ";
              $result2 = $db->prepare(self::$sql);
              $result2->execute();
              self::logout();
        }
    }
    
    public function getStatus($userId){
        $db = Db::getConnection();
        self::$sql = 'SELECT * FROM status WHERE id = :id';
        $result = $db->prepare(self::$sql);
        $result->bindParam(':id', $userId, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
 
        return $result->fetch();
    }
    
    public function timeout(){
       $ip = $_SERVER['REMOTE_ADDR'];
       $db = Db::getConnection();
       
       self::$sql = 'SELECT * FROM blockcheck WHERE ip = :ip';
       $result = $db->prepare(self::$sql);
       $result->bindParam(':ip', $ip,PDO::PARAM_STR);
       $result->setFetchMode(PDO::FETCH_ASSOC);
       $result->execute();
       $block = $result->fetch();
      
          
       if($block['count'] == 0){
           $block['count'] = $block['count'] + 1;
           self::$sql = "INSERT INTO blockcheck(ip,time,count) VALUES(:ip,NOW(),:count)";
           $result1 = $db->prepare(self::$sql);
           $result1->bindParam(':ip', $ip, PDO::PARAM_STR);
           $result1->bindParam(':count', $block['count'], PDO::PARAM_INT);
           $result1->execute();

           return TRUE;
       }else{           
           $block['count'] = $block['count'] + 1;                       
           if($block['count'] < 5){
               self::$sql = "UPDATE blockcheck SET count = :count,time = NOW() WHERE ip = :ip";
               $tmp = $db->prepare(self::$sql);
               $tmp->bindParam(':count', $block['count'], PDO::PARAM_INT);
               $tmp->bindParam(':ip', $ip, PDO::PARAM_STR);               
               $tmp->execute();
               return TRUE;
              
           }
           
           else{
               $time = time() - strtotime($block['time']);
               if($time < 300){
                       return FALSE;
               }
              
               else{
                   self::$sql = "DELETE FROM blockcheck WHERE ip = :ip";
                   $tmp1 = $db->prepare(self::$sql);
                   $tmp1 ->bindParam(':ip', $ip, PDO::PARAM_STR);
                   $tmp1->execute();
                   return TRUE;
               }
            }
       } 

    }
    
    public function checkTimeout(){
        $ip = $_SERVER['REMOTE_ADDR'];
        $db = Db::getConnection();
       
       self::$sql = 'SELECT * FROM blockcheck WHERE ip = :ip';
       $result = $db->prepare(self::$sql);
       $result->bindParam(':ip', $ip,PDO::PARAM_STR);
       $result->setFetchMode(PDO::FETCH_ASSOC);
       $result->execute();
       //block - содержит данные о пользователе с данным ip.
       $block = $result->fetch();
       if(empty($block['ip']) || $block['count'] <= 3 ){
           return TRUE;
       }
       else{
           $time = time() - strtotime($block['time']);
            if($time < 300){
                return FALSE;
            }else{
                self::$sql = "DELETE FROM blockcheck WHERE ip = :ip";
                $tmp1 = $db->prepare(self::$sql);
                $tmp1 ->bindParam(':ip', $ip, PDO::PARAM_STR);
                $tmp1->execute();
                return TRUE;
            }            
        } 
    }
}

